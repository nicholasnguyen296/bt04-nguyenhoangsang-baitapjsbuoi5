// Bài tập 1
function kiemTraDiemLiet(diem, diem2, diem3) {
  if (diem <= 0 || diem2 <= 0 || diem3 <= 0) {
    return true;
  } else {
    return false;
  }
}
function tinhDiem() {
  var diemThuNhat = document.getElementById("txt-o-1").value * 1;
  var diemThuHai = document.getElementById("txt-o-2").value * 1;
  var diemThuBa = document.getElementById("txt-o-3").value * 1;
  var tongDiem = diemThuNhat + diemThuHai + diemThuBa;
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var result = null;
  var khuVuc = document.getElementById("txt-khu-vuc").value * 1;
  var doiTuong = document.getElementById("txt-doi-tuong").value * 1;
  tongDiem += khuVuc + doiTuong;
  if (kiemTraDiemLiet(diemThuNhat, diemThuHai, diemThuBa)) {
    result = `Bạn đã rớt. Do có tổng điểm nhỏ hơn hoặc bằng 0`;
  } else {
    if (tongDiem >= diemChuan) {
      result = `Bạn đã đậu.Tổng điểm : ${tongDiem}`;
    } else {
      result = `Bạn đã rớt.Tổng điểm:${tongDiem}`;
    }
  }
  document.getElementById("result").innerHTML = result;
}
// End bài tập 1

// Bài tập 2
function tinhTienSoKw(soKw, tien) {
  var soTien = 0;
  soTien = soKw * tien;
  return soTien;
}

function tinhTienDien() {
  var hoTen = document.getElementById("txt-ho-ten").value;
  var soKw = document.getElementById("txt-so-kw").value * 1;
  var tien50KwDau = 500;
  var tien50KwSau = 650;
  var tien100Kw = 850;
  var tien150Kw = 1100;
  var tienConLai = 1300;
  var tongTienDien = 0;
  if (soKw <= 50) {
    tongTienDien = tinhTienSoKw(soKw, tien50KwDau);
  }
  if (soKw >= 51) {
    tongTienDien =
      tinhTienSoKw(50, tien50KwDau) + tinhTienSoKw(soKw - 50, tien50KwSau);
  }
  if (soKw >= 100) {
    tongTienDien =
      tinhTienSoKw(50, tien50KwDau) +
      tinhTienSoKw(50, tien50KwSau) +
      tinhTienSoKw(soKw - 100, tien100Kw);
  }
  if (soKw >= 150) {
    tongTienDien =
      tinhTienSoKw(50, tien50KwDau) +
      tinhTienSoKw(50, tien50KwSau) +
      tinhTienSoKw(50, tien100Kw) +
      tinhTienSoKw(soKw - 150, tien150Kw);
  }
  if (soKw > 200) {
    tongTienDien =
      tinhTienSoKw(50, tien50KwDau) +
      tinhTienSoKw(50, tien50KwSau) +
      tinhTienSoKw(50, tien100Kw) +
      tinhTienSoKw(50, tien150Kw) +
      tinhTienSoKw(soKw - 200, tienConLai);
  }
  document.getElementById(
    "tongTien"
  ).innerHTML = `Họ tên: ${hoTen} ;Tiền điện : ${tongTienDien.toLocaleString()}Đồng`;
}

// End bài tập 2
